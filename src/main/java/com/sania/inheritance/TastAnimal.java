/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sania.inheritance;

/**
 *
 * @author User
 */
public class TastAnimal {
    public static void main(String[] args){
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        separator();
        
        Dog dang = new Dog("Dang","Black&White");
        dang.speak();
        dang.walk();
        separator();
        
        Cat zero = new Cat("zero","Orange");
        zero.speak();
        zero.walk();
        separator();
        
        Duck som = new Duck("som","Orange");
        som.speak();
        som.walk();
        som.fly();
        separator();
        
        Dog to = new Dog("To","Brown");
        to.speak();
        to.walk();
        separator();
        
        Dog mome = new Dog("Mome","White");
        mome.speak();
        mome.walk();
        separator();
        
        Dog bat = new Dog("Bat","White");
        bat.speak();
        bat.walk();
        separator();
        
        Duck gabgab = new Duck("GabGab","Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        separator();
        
        System.out.println("Som is Animal: "+(som instanceof Animal));
        System.out.println("Som is Duck: "+(som instanceof Duck));
        System.out.println("Som is Object: "+(som instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(animal instanceof Animal));
        System.out.println("Gabgab is Animal: "+(gabgab instanceof Animal));
        System.out.println("Gabgab is Duck: "+(gabgab instanceof Duck));
        System.out.println("TO is Dog: "+(to instanceof Dog));
        System.out.println("Animal is Duck: "+(animal instanceof Duck));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = som;
        ani2 = zero;
        
        System.out.println("Ani1: som is Duck "+(ani1 instanceof Duck));
        
        Animal[] animals = {dang,zero,som,to,mome,bat,gabgab};
        for(int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            separator();
        }
    }

    private static void separator() {
        System.out.println("--------------------");
    }
}
